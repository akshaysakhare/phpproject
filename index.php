<?php 
 if(isset($_POST['submit'])){
     $state=$_POST['state'];
    switch ($state) {

    case "Maharashtra":
    $state_name= "Maharashtra";
    $state_capital= "Mumbai";
    $language= "Marathi";
        break;

    case "Karnataka":
        $state_name= "Karnataka";
        $state_capital= "Bangalore";
        $language= "Kannada";
           break;

    case "Andhra":
    $state_name= "Andhra Pradesh";
    $state_capital= "Amaravati";
    $language= "Telugu";
        break;

    case "Bihar":
        $state_name= "Bihar";
        $state_capital= "Patna";
        $language= "Hindi";
           break;
    
           case "Goa":
           
            $state_name= "Goa";
            $state_capital= "Panaji";
            $language= "Konkani";
            break;

    default:
      echo '<div class="alert alert-danger" role="alert">';
            echo "Entered State not found, Try again!";
       echo '</div>';
    }
 }
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Counter</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Urbanist:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <link rel="stylesheet" href="style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    </head>
    <body>


<div class="container pt-4">
    <div class="form-responsive">
  
        <form action="" method="post">
            <div class="form-group">
            <h4>State Information</h4>
              
         <input type="text" name="state" class="form-control" placeholder="enter any state name" value="Goa"/><br>
                
         <input type="submit" class="btn btn-primary m-2" name="submit" value="submit"/>                
            </div>
        </form>
    </div>
    
        <table class="table">
            <thead>
              <tr>
                 <th scope="col">State Name</th>
                <th scope="col">Capital</th>
                <th scope="col">Language</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                
                <td><?php echo $state_name; ?></td>
                <td><?php echo $state_capital; ?></td>
                <td><?php echo $language; ?></td>
              </tr>
            </tbody>
        </table>
     </div>
    </body>
</html>